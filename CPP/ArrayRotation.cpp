#include <iostream>
using namespace std;

// Formula:
// Left rotation  arr[ ( i + rotation) % size ];
// Right rotation arr[ ( i + size - rotation ) % size ]


void printArray(int  arr[],int size)
{
	for(int i = 0; i <  size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << endl;
}

int main1() {
	int size = 5;
	int arr[5] = { 1,2,3,4,5 };
	int rotation;
	cout << "Enter number of rotations : ";
	cin >> rotation;

	printArray(arr , size);

	for(int i = 0; i < rotation ; i++)
	{
		int temp = arr[0], j;
		for(j = 0; j < size -1 ; j++)
			arr[j] = arr[j + 1];

		arr[j] = temp;
	}

	printArray(arr, size);
	return 0;
}

int main() {
	int size = 5;
	int arr[5] = { 1,2,3,4,5 };
	int rotation;
	cout << "Enter number of rotations : ";
	cin >> rotation;
	cout << "Original Array : ";
	printArray(arr , size);
	cout<<"right rotation : ";
	for(int i = 0; i < size ; i++)
	{
		cout << arr[(i + size - rotation) % size]<<" ";
	}
	cout <<endl;
	cout<<"Left rotation  : ";
	for(int i = 0; i < size ; i++)
	{
		cout << arr[(i + rotation) % size]<<" ";
	}
	cout <<endl;
	return 0;
}
