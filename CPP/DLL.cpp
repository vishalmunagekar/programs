#include <iostream>
using namespace std;

class node
{
private:
	int data;
	node *prev;
	node *next;
public:
	node(int data)
	{
		this->data = data;
		this->prev = NULL;
		this->next = NULL;
	}
};

class list
{
private:
	node *head;
public:
	list()
	{
		this->head = NULL;
	}
	bool isEmpty()
	{
		return (this->head == NULL);
	}
	void addLastNode(int data)
	{
		node *newnode = new node(data);
		if(isEmpty())
		{
			this->head = newnode;
		}
		else{
			node *trav = head;
			while(trav->next != NULL)
				trav = trav->next;
			newnode->prev = trav;
			trav->next = newnode;
		}
	}

	void addFirstNode(int data)
	{
		node *newnode = new node(data);
		if(isEmpty())
		{
			head = newnode;
		}
		else
		{
			newnode->next = head;
			head->next = newnode;
			head = newnode;
		}
	}
	void printList()
	{
		node *trav = head;
		while(trav != NULL)
		{
			trav = trav->next;
			cout << trav->data << " ";
		}
	}
	void findMiddleNode(node *head)
	{
	    node *slow = head;
	    node *fast = head;
	    
	    if(head != NULL){
	        while(fast != NULL && fast->next != NULL){
	            fast = fast->next->next;
	            slow = slow->next;
	        }
	        cout << slow->data <<endl;
	    }
	}
};

int main() {
	return 0;
}
