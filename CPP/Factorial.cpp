#include <iostream>
using namespace std;

int main() {
	int n, i, f=1; //important to assign 1 to factorial variable
	cout << "Enter a Number to find Factorial :";
	cin >> n;
	for(i = 1; i <= n; i++)
	{
		f *= i; 			//f = f * i
	}
	cout << "Factorial of "<<n<<" is "<<f;
	return 0;
}


//by recursion
float factorial(int n)
{
	if(n >=1)
		return n * factorial(n - 1);
	else
		return 1;
}


int main() {
	int n;
	cout << "Enter a number to find out Factorial : ";
	cin >> n;
	cout << "Factorial of "<<n<<" is "<<factorial(n);
	return 0;
}
