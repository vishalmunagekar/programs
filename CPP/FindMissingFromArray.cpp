#include <iostream>
using namespace std;

int main() {
	int arr[10] = {1, 2, 3, 5, 6, 7, 8, 9, 10};
	int n =  10;
	int total = 0;
	int sum = n * ( n + 1) / 2;
	for(int i = 0 ; i < n; i++)
	{
		total = total + arr[i];
	}
	int missing = sum - total;
	cout << "Missing : "<< missing;
	return 0;
}
