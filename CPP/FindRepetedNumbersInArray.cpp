#include <iostream>
using namespace std;

int main() {
	int arr[] = {1,2,3,4,5,6,7,5,8,9,4,6};
	int n = sizeof(arr) / sizeof(arr[0]);
	for(int i = 0; i < n ; i++)
	{
		for(int j = i + 1; j < n; j++) //start this loop by i+1
		{
			if(arr[i] == arr[j])
			{
				cout << arr[i] << " ";
			}
		}
	}
	return 0;
}
