#include <iostream>
using namespace std;

int main() {
	string str;
	int vowles=0, consonants=0, space=0, digit=0;
	cout << "Enter Somthing : ";
	getline(cin, str); // for getting multiple words from user (space);
	for(int i = 0; i < str.length() ; i++)
	{
		if(str[i] == 'a' || str[i] == 'e' || str[i] == 'i' || str[i] == 'o' || str[i] == 'u' ||
		   str[i] == 'A' || str[i] == 'E' || str[i] == 'I' || str[i] == 'O' || str[i] == 'U')
		{
			vowles++;
		}
		else if(str[i] >= 'a' && str[i] <= 'z' || str[i] >= 'A' && str[i] <= 'Z')
		{
			consonants++;
		}
		else if(str[i] == ' ')
		{
			space++;
		}
		else if(str[i] >= 48 && str[i] <= 57)
		{
			digit++;
		}

	}
	cout << str<<endl;
	cout << "Count of vewels is : "<< vowles<<endl;
	cout << "Count of consonants is : "<< consonants<<endl;
	cout << "Count of space is : "<< space<<endl;
	cout << "Count of digit is : "<< digit;
	return 0;
}
