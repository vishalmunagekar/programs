#include <iostream>
using namespace std;
// 1. 454
// 2. 8998
// 3. 2772

int main() {
	int n, sum = 0, temp, r;
	cout << "Enter a number : ";
	cin >> n;
	temp = n;
	while(n > 0)
	{
		r = n % 10;
		sum = (sum * 10) + r;
		n = n / 10;
	}
	if(temp == sum)
		cout << "Palindrome Number";
	else
		cout << "Not a Palindrome Number";
}
