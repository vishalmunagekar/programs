#include <iostream>
using namespace std;

//selection sort
int main1() {
	int arr[10] = {30, 40, 70, 10, 20, 90, 100, 50, 60, 80};
	cout << "Before sort : ";
	for(int i = 0 ; i < 10; i++)
			cout << arr[i] << " ";
	cout<<endl;
	for(int i = 0 ; i < 10; i++)
	{
		for(int j = i + 1; j < 10 ; j++) //Smaller number will come at 1st position in 1st iteration
		{
			if(arr[i] > arr[j])
			{
				int temp = arr[i];
				arr[i] = arr[j];
				arr[j] = temp;
			}
		}
	}
	cout << "After sort  : ";
	for(int i = 0 ; i < 10; i++)
		cout << arr[i] << " ";
	return 0;
}

//Bubble sort
int main() {
	int arr[10] = {30, 40, 70, 10, 20, 90, 100, 50, 60, 80};
	cout << "Before sort : ";
	for(int i = 0 ; i < 10; i++)
			cout << arr[i] << " ";
	cout<<endl;
	for(int i = 0 ; i < 10; i++)
	{
		for(int j = 0; j < 10-i-1 ; j++) //larger number will go at last position in 1st iteration
		{
			if(arr[j] > arr[j+1])
			{
				int temp = arr[j];
				arr[j] = arr[j+1];
				arr[j+1] = temp;
			}
		}
	}
	cout << "After sort  : ";
	for(int i = 0 ; i < 10; i++)
		cout << arr[i] << " ";
	return 0;
}
