#include <iostream>
#include <stdlib.h>
using namespace std;

class Stack
{
public:
	int arr[5];
	int top;

	Stack()
	{
		top = -1;
	}

	bool isEmpty()
	{
		if(top == -1)
			return true;
		else
			return false;
	}
	bool isFull()
	{
		if(top == 4)
			return true;
		else
			return false;
	}
	int push(int val)
	{
		if(isFull()){
			return 1;
		}
		else{
			top++;
			arr[top] = val;
		}
		return 0;
	}

	int pop()
	{
		if(isEmpty()){
		}
		else{
			int val = arr[top];
			arr[top] = 0;
			top--;
			return val;
		}
		return -1;
	}

	int peek()
	{
		if(isEmpty())
			return -1;
		else
			return arr[top];
	}
	void print()
	{
		for(int i = 0; i < 5; i++)
		{
			cout << arr[i]<< " ";
		}
		cout<<endl;
	}
};


int main() {
	int c;
	int val;
	Stack stack;
	while(1)
	{
		cout << "1. Push"<<endl;
		cout << "2. Pop"<<endl;
		cout << "3. Peek"<<endl;
		cout << "4. Print"<<endl;
		cout << "5. Exit"<<endl;
		cout << "Enter your choice : ";
		cin >> c;
		switch(c)
		{
		case 1:
			cout << "Enter value to push";
			cin >> val;
			if(stack.push(val))
				cout << "Stack overflow"<<endl;
			else
				cout << "Value added!!!"<<endl;
			break;
		case 2:
			val = stack.pop();
			if(val == -1)
				cout << "Stack is Empty"<<endl;
			else
				cout << val<<endl;
			break;
		case 3:
			val = stack.peek();
			if(val == -1)
				cout << "Stack is empty"<<endl;
			else
				cout << val<<endl;
			break;
		case 4:
			stack.print();
			break;
		case 5:
			cout << "Exited..!!!";
			exit(0);
		}
	}
	return 0;
}
