#include <iostream>
using namespace std;

int main() {
	int n, s, i, j;
	cout << "Enter number of rows: ";
	cin >> n;
	for(i = n; i >= 1; i--)
	{
		for(s = i; s < n; s++)   //for loop to put space
			cout << " ";
		for(j = 1; j <= i; j++)  //for loop for displaying star
			cout << "* ";
		cout << "\n";  			 // ending line after each row
	}
	return 0;
}


// Enter number of rows: 6
// * * * * * *
//  * * * * *
//   * * * *
//    * * *
//     * *
//      *
