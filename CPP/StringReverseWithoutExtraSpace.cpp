#include <iostream>
using namespace std;

string reverseString1(string str)
{
	char ch;
	int start = 0;
	int end = str.length() - 1;
	while(start < end)
	{
		ch = str[start];
		str[start] = str[end];
		str[end] = ch;

		start++;
		end--;
	}
	return str;
}

string reverseString2(string str)
{
	int start = 0;
	int end = str.length() - 1;
	while(start < end)
	{
		str[start] ^= str[end];
		str[end] ^= str[start];
		str[start] ^= str[end];

		start++;
		end--;
	}
	return str;
}

int main() {
	cout << reverseString1("ABCDEFGHIJKLMNO")<<endl;
	cout << reverseString2("VISHALMUNAGEKAR");
	return 0;
}
