#include <iostream>
#include <ctime>
using namespace std;

int main() {
	time_t now = time(0); //now is time_t type obj now it is pointing to System date and time

	char *date = ctime(&now); //now convert it into String format

	cout <<"System date is :"<< date;
	return 0;
}
