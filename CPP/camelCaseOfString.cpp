#include <iostream>
using namespace std;

string camelCase(string str)
{
	string newstr = "";
	for(int i = 0; i < str.length(); i++)
	{
		str[i] = tolower(str[i]);
		if(str[i] == ' ')
		{
			str[i+1] = toupper(str[i+1]);
			i++;
		}
	}
	for(int i = 0; i < str.length(); i++)
	{
		if(str[i] == ' ')
		{
			i++;
		}
		newstr = newstr + str[i];
	}
	return newstr;
}

int main() {
	string str = "HelLo vIsHaL munGeKaR iS hErE";
	cout << camelCase(str);
	return 0;
}
