int main(void)
{
    int size = 9;
	int arr[size] = {50,20,70,10,80,90,30,60,40};
	merge_sort(arr, 0, size-1);
	return 0;
}

void merge_sort(int arr[], int left, int right)
{
	if( left >= right )
		return;

	int mid = (left+right)/2;
	merge_sort(arr,left, mid);
	merge_sort(arr,mid+1, right);
	merge(arr,left,mid,right);
}

void merge(int arr[],int left,int mid,int right)
{
	int i = left;
	int j = mid+1;
	int k = 0;
	int size = (right-left)+1;
	int *temp = new int[size];

	while( i <= mid && j <= right )
	{
		if( arr[i] < arr[j] )
			temp[k++] = arr[i++];
		else
			temp[k++] = arr[j++];
	}

	//copy remaining ele's from left sub array into temp array
	while( i <= mid )
		temp[k++] = arr[i++];

	//copy remaining ele's from right sub array into temp array
	while( j <= right )
		temp[k++] = arr[j++];

	//copy all ele's from temp array into an original array
	i=left;
	k=0;
	while( i <= right )
		arr[i++] = temp[k++];

	delete []temp;
	temp = NULL;
}