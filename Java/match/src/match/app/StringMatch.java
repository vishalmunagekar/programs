package match.app;


public class StringMatch {

	public static void main(String[] args) {
		String str1 = "Alive";
		String str2 = "plive";
		System.out.println(isStringMatch(str1, str2));
	}
	
	public static boolean isStringMatch(String str1, String str2)
	{
		StringBuffer sb = new StringBuffer(str1);
		for(int i = 0; i < str2.length(); i++)
		{
			if(contains(sb, str2.charAt(i)))
			{
				return false;
			}
		}
		return true;
	}
	
	public static boolean contains(StringBuffer sb, char ch)
	{
		for(int i = 0; i < sb.length(); i++)
		{
			if(sb.charAt(i) != ch)
				return false;
		}
		return true;
	}
}
