function init() {
var content = document.querySelector("#reporter"),
    listeners = [],
    listenerEnabled = false,
    printIt = true,
    lastevent;

var loadInfo = function (msg, eventObj) {
    content.insertAdjacentHTML("afterbegin", msg + " -- event type : " + eventObj.type + " -- target Object : " + eventObj.target.nodeName + "<br>");
};

var addListeners = function () {
    var keyDownHandler = function (e) {
        loadInfo("A key was pressed: " + e.keyCode + " -- " + e.key, e);
        if (e.keyCode === 83 && e.ctrlkey) {
            toggleEventListeners();
        }
    };
    document.addEventListener("keydown", keyDownHandler);
    listeners.push(keyDownHandler, "keydown");

    var mouseMoveHandler = function (e) {
        lastevent = e;
        if (printIt) {
            printIt = false;
            loadInfo("mouse move recorded at coordinates: " + e.pageX + ", " + e.pageY, e);
            setTimeout(function () {
                printIt = true;
            }, 500);
        }
    };

    document.addEventListener("mousemove", mouseMoveHandler);
    listeners.push(mouseMoveHandler, "mousemove");

    listenerEnabled = true;
};

var removeListeners = function () {
    while (listeners.length > 0) {
        document.removeEventListener(listeners.pop(), listeners.pop());
    }
};

var toggleEventListeners = function () {
    if (listenerEnabled) {
        removeListeners();
        console.log("Event Listeners Removed");
    } else {
        addListeners();
        console.log("Listeners added");
    }
};

toggleEventListeners();
}

window.addEventListener('load', init);