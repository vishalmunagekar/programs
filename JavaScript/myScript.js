document.write("<h2>=========================== Vowels & Consonents in js ====================================</h2>");

function vowelsAndConsonants( s ){
    var vowels = "aeiouAEIOU";
    var v = new Array();
    var c = new Array();
    for(let i =0; i < s.length; i++)
    {
        if(vowels.indexOf(s[i]) != -1){
            v.push(s[i]);
        }
        if(vowels.indexOf(s[i]) == -1)
        {
            c.push(s[i]);
        }
    }
    document.writeln(v);
    document.writeln(c);
}

vowelsAndConsonants("vishalmunagekar");

var str = "munagekar";
var vowels = "aeiouAEIOU";
for(let i =0; i < str.length; i++)
    {
        document.writeln(vowels.indexOf(str[i])); // if character is present in String then it will 
                                                  // return index & if not then return -1
    }


document.write("<h2>=========================== Objects & Class in js ====================================</h2>");
/*
var emp =
{
    emp_id: 1,
    emp_name: "vishal",
    emp_salary: 25000,
    emp_dept: "Web Developer",

    printEmpDetails : function(){                           //object function
        document.write("<h1>Emp ID : " + this.emp_id + "</h2>");
        document.write("<h1>Emp Name : " + this.emp_name +"</h2>");
        document.write("<h1>Emp Salary : " + this.emp_salary +"</h2>");
        document.write("<h1>Emp Department : "+ this.emp_dept +"</h2>");
    }
};

emp.printEmpDetails(); calling methods of object

*/
class emp {
    constructor(emp_id, emp_name, emp_salary, emp_dept) {
        this.emp_id = emp_id;
        this.emp_name = emp_name;
        this.emp_salary = emp_salary;
        this.emp_dept = emp_dept;

        this.printEmpDetails = function () {
            document.write("<h1>Emp ID : " + this.emp_id + "</h2>");
            document.write("<h1>Emp Name : " + this.emp_name + "</h2>");
            document.write("<h1>Emp Salary : " + this.emp_salary + "</h2>");
            document.write("<h1>Emp Department : " + this.emp_dept + "</h2>");
        };
    }
}

var E1 = new emp(1, "vishal", 36000, "Web Developer");
var E2 = new emp(2, "Aniket", 36000, "App Developer");

E1.printEmpDetails();
E2.printEmpDetails();

document.write("<h2>=========================== Arrays ====================================</h2>");

var cars = ["BMW", "Volvo", "Audi", "MARUTI"]; // declearation of array
var arr = new Array("Mango", "Apple", "Banana"); //another technique of declearing array

arr.push(200);
arr.pop();
arr.length;
cars.push("vishal");


document.write("<h2>" + cars + "</h2>"); // this will print all values
document.write("<h2>" + cars[1] + "</h2>"); // this will print specific index values

document.write("<h1>using for loop</h1>") //using for  loop
for (let i = 0; i < cars.length; i++)
    document.write("<h2>" + cars[i] + "</h2>");

document.write("<h2>=========================== functions ====================================</h2>");

function addTwoNumbers(a, b) //not required parameter type bcoz js is loosly cupled
{
    var total = a + b;
    //document.write("<h1>Addition of two numbers : "+ total +"</h1>");
    return total;
}

var total = addTwoNumbers(5, 4); //calling of function with two arguments
document.write("<h1>Addition of two numbers : " + total + "</h1>");

document.write("<h2>================================ for loop =====================================</h2>");

var num = 5;
for (let x = 1; x <= 10; x++) {
    document.write("<h1>" + 5 * x + "</h1>")
}

document.write("<h2>================================= switch case ====================================</h2>");

document.write("<h2>Switch case :</h2>");
var day = 1;
switch (day) {
    case 1:
        document.write("<h2>Sunday</h2>");
        break;
    case 2:
        document.write("<h2>Monday</h2>");
        break;
    case 3:
        document.write("<h2>Tuesday</h2>");
        break;
    case 4:
        document.write("<h2>Wednesday</h2>");
        break;
    case 5:
        document.write("<h2>Thursday</h2>");
        break;
    case 6:
        document.write("<h2>Friday</h2>");
        break;
    case 7:
        document.write("<h2>Saturday</h2>");
        break;
    default:
        document.write("<h2>Wrong Choice!!</h2>");
        break;
}

document.write("<h2>=====================================================================</h2>");

document.write("<h1> Find number is Even or not</h1>");
var x = 7;
if (x % 2 == 3) {
    document.write("<h1>" + x + " is Even Number</h1>");
}
else {
    document.write("<h1>" + x + " is Odd Number</h1>");
}

document.write("<h2>================================ if else =====================================</h2>");

var num = 17;
var name = "Vishal";
var boolean = true;
document.write("<h1>" + num + "</h1>");
document.write("<h1>" + name + "</h1>");
document.write("<h1>" + num + "</h1>");
document.write("<h1>" + boolean + "</h1>");

document.write("<h2>=================================== print on page ==================================</h2>");

document.write("<h1>Hello vishal</h1>");
document.write("<h2>Hello Munagekar</h2>");

//this is a comment
/*
this
is
Multiline 
comment
*/

document.write("<h2>=====================================================================</h2>");