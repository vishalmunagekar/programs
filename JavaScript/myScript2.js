document.write("<h3>=========================== DOM Manipulations ====================================</h3>");
function btn1Click() {
    //alert("Button Clicked!!!");
    //document.getElementById("heading2").innerHTML = "Vishal Munagekar";
    var str = document.getElementById("heading2").innerHTML;
    //var str = document.getElementById("heading2").innerText;
    alert(str);
}

function btn2Click() {
    var str1 = document.getElementById("input1").value;
    alert("Value in input box is : " + str1);

}

function fun() {
    var btn1 = document.getElementById("rb1");
    var btn2 = document.getElementById("rb2");
    var btn3 = document.getElementById("rb3");
    var btn4 = document.getElementById("rb4");
    if (btn1.checked == true)
        alert(btn.value);
    else if (btn2.checked == true)
        alert(btn2.value);
    else if (btn3.checked == true)
        alert(btn3.value);
    else if (btn4.checked == true)
        alert(btn4.value);
    else
        alert("Not Selected!!");
}

function fun2() {
    var select = document.getElementById("selectbox");
    //alert(select.options[0].value);
    //alert(select.options[1].value);
    alert(select.options[select.selectedIndex].value);
}

function bulbOn() {
    document.getElementById("img1").src = "/bulb-on.png";
}
function bulbOff() {
    document.getElementById("img1").src = "/bulb-off.png";
}

function validate() {
    var uname = document.getElementById("uname");
    var pass = document.getElementById("pass");

    if (uname.value.trim() == "" || pass.value.trim() == "" || pass.value.length < 9 ) {
        alert("No blank values allowed!!")
        return false;
    }
    else{
        return true;
    }
}

function onlyChar()
{
    var uname = document.getElementById("uname1").value;
    var regx = /^[a-z]$+/i;
    var regx2 = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/; //IP address
    if(regx.test(uname))
        alert("Valid");
    else
        alert("Invalid");
}