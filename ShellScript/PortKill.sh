#!/bin/bash
if [[ ( $1 -eq "" ) ]]
then
echo "Port Number Is required"
else
    pid=$(lsof -t -i:$1)
    if [[ ( $pid -eq "" ) ]]
    then
    echo "No Process is Running On Port : $1"
    else
    kill $pid
    echo "$pid process running on Port $1 is Killed Successfully"
    fi
fi