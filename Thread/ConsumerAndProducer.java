package com.app;

class ProducerConsumer
{
	int balance = 50;

	public ProducerConsumer() {

	}

	public synchronized void produce() throws InterruptedException
	{
		while(true) {
			if(balance >= 100)
				wait();
			balance += 10;
			System.out.println(balance);

			notify();

			Thread.sleep(1500);
		}
	}

	public synchronized void consume() throws InterruptedException
	{
		while(true) {
			if(balance <= 0)
				wait();
			balance -= 10;
			System.out.println(balance);

			notify();

			Thread.sleep(1500);
		}
	}
}


public class Main {

	public static void main(String[] args) throws InterruptedException {
		ProducerConsumer pc = new ProducerConsumer();

		Thread producer = new Thread(new  Runnable() {

			@Override
			public void run() {
				try {
					pc.produce();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});

		Thread consumer = new Thread(new  Runnable() {

			@Override
			public void run() {
				try {
					pc.consume();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}				
			}
		});

		producer.start();
		consumer.start();

		producer.join();
		consumer.join();
	}

}
