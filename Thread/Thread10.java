package org.sunbeam.kdac;

class CThread extends Thread
{
	public CThread( String name )
	{
		this.setName(name);
	}
	@Override
	public void run() 
	{
		System.out.println(Thread.currentThread().toString());
	}
}
public class Program 
{
	public static void main(String[] args) throws InterruptedException 
	{
		System.out.println(Thread.currentThread().toString());
		
		Thread thread = new CThread("User Thread");
		//thread.start();
		thread.run();
	}
}
