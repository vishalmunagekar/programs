package org.sunbeam.kdac;

class CThread implements Runnable
{
	private Thread thread;
	public CThread( String name ) 
	{
		
		this.thread = new Thread( this );
		this.thread.setName(name);
		this.thread.start();
	}
	@Override
	public void run() 
	{
		//TODO : Business logic
	}
}
public class Program 
{
	public static void main(String[] args) 
	{
		CThread th1 = new CThread("Thread#1");
	}
}
