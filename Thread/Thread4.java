package org.sunbeam.kdac;

class CThread extends Thread
{
	public CThread( String name ) 
	{
		this.setName(name);
		this.start();
	}
	@Override
	public void run() throws RuntimeException
	{
		try 
		{
			for( int count = 1; count <= 10; ++ count )
			{
				System.out.println(Thread.currentThread().getName()+"	:	"+count);
				Thread.sleep(500);
			}
		} 
		catch (InterruptedException cause)
		{
			throw new RuntimeException(cause);
		}
	}
}
public class Program 
{
	public static void main(String[] args)throws Exception
	{
		CThread th1 = new CThread( "User Thread#1" );
		th1.join();
		
		CThread th2 = new CThread( "User Thread#2" );
		th2.join();
		
		CThread th3 = new CThread( "User Thread#3" );
		th3.join();
		
		CThread th4 = new CThread( "User Thread#4" );
		th4.join();
		
		CThread th5 = new CThread( "User Thread#5" );
		th5.join();
	}
}
