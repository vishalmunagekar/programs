package org.sunbeam.kdac;

class CThread extends Thread
{
	public CThread( String name ) 
	{
		this.setName(name);
		this.start();
	}
	@Override
	public void run() 
	{
		Thread thread = Thread.currentThread();
		System.out.println(thread.getName()+"	"+thread.getPriority());
	}
}
public class Program 
{
	public static void main(String[] args)throws Exception
	{
		Thread thread = Thread.currentThread();
		thread.setPriority(Thread.NORM_PRIORITY + 6 ); //IllegalArgumentException
		
		System.out.println(thread.getName()+"	"+thread.getPriority());
		CThread th1 = new CThread( "User Thread" );
	}
}
