package org.sunbeam.kdac;

public class Program 
{
	private String message = "Hello";
	public void print( ) throws InterruptedException
	{
		synchronized( this )
		{
			System.out.println(message);
			//message.wait(); //IllegalMonitorStateException
			this.wait( 1000 ); //OK
		}
	}
	public static void main(String[] args) throws InterruptedException 
	{
		Program p = new Program();
		p.print();
	}
}
