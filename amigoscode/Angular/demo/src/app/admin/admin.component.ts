import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  allStudents:any;
  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.getAllStudentsAdmin().subscribe((result)=>{
      this.allStudents = result;
    })
  }

}
