import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { UserComponent } from './user/user.component';


const routes: Routes = [
  {path:"",component:HomeComponent},
  {path:"User",component:HomeComponent},
  {path:"UserHome",component:UserComponent},
  {path:"AdminHome",component:AdminComponent},
  {path:"Login",component:LoginComponent},
  {path:"**",component:HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const ApplicationRouts = [
  HomeComponent,
  LoginComponent,
  AdminComponent,
  UserComponent
]
