import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http:HttpClient) { }

  getAllStudents(){
    return this.http.get("http://localhost:9090/api/v1/students");
  }

  getStudentDetails(sid:number)
  {
    return this.http.get("http://localhost:9090/api/v1/students/"+ sid);
  }

  getAllStudentsAdmin(){
    return this.http.get("http://localhost:9090/management/api/v1/students");
  }

  deleteStudentAdmin(sid:number){
    return this.http.delete("http://localhost:9090/management/api/v1/students/"+sid);
  }

  getCityNamesByDistrict(district:string){
    return this.http.get("https://indian-cities-api-nocbegfhqg.now.sh/cities?District=" + district);
  }
}
