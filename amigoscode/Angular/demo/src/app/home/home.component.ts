import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  districts:string[] = ['Kolhapur','Satara', 'Pune', 'Mumbai', 'Nashik', 'Solapur'];
  cities:any[];
  constructor(private dataService:DataService) { }

  ngOnInit(): void {
  }

  getCities(dist){
    console.log(dist);
    this.dataService.getCityNamesByDistrict(dist).subscribe((result:any[])=>{
    this.cities = result;
    console.log(this.cities);
    })
  }

}
