import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TokenDetails } from './interface/TokenDetails';

@Injectable({
  providedIn: 'root'
})
export class Interceptor implements HttpInterceptor {

  constructor() {
    //console.log("Interceptor loaded...");
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (req.url.indexOf('login') !== -1) {
     // console.log(req.url);
      return next.handle(req);
    }
    else {
      return next.handle(this.addToken(req));
    }
  }

  addToken(req: HttpRequest<any>) {
    return req.clone({
      headers: req.headers.set('Authorization',
        'Bearer ' + localStorage.token)
    });
  }
}
