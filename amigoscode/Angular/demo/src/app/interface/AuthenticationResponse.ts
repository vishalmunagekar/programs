export interface AuthenticationResponse {
  authenticationToken:string;
  expiresAt:string;
  username:string;
}
