export interface Authorization {
  authority: string;
}

export interface TokenDetails {
  sub: string;
  Authorization: Authorization[];
  iat: number;
  exp: number;
}
