import { HttpHeaderResponse, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { AuthenticationResponse } from '../interface/AuthenticationResponse';
import { UserService } from '../user.service';
//import { MD5, enc } from "crypto-js";
import { JwtHelperService } from '@auth0/angular-jwt';
import { TokenDetails } from '../interface/TokenDetails'
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  msg:string;
  helper = new JwtHelperService();
  tokenDetails:TokenDetails;
  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
    observe: 'response' as 'response'
  };

  constructor(private userService : UserService,
    private router: Router) { }

  ngOnInit(): void {
  }

  login(credentials:any){
    this.userService.login(credentials)
    .subscribe((res:AuthenticationResponse)=>{
      localStorage.setItem("token",res.authenticationToken);
      this.tokenDetails = this.helper.decodeToken(res.authenticationToken);
      if(this.tokenDetails.Authorization[0].authority === "ROLE_USER")
      {
        this.router.navigate(['UserHome']);
      }
      else{
        this.router.navigate(['AdminHome']);
      }
    }, (error)=>{
      this.msg = "Somthing went Wrong...!!";
    })
  }
}
