import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  allStudents:any;
  constructor(
    private dataService:DataService
  ) { }

  ngOnInit(): void {
    this.dataService.getAllStudents().subscribe((result:any)=>{
      this.allStudents = result;
    })
  }

}
