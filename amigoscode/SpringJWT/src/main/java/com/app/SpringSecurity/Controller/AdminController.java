package com.app.SpringSecurity.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("management/api/admin")
public class AdminController {
	private static final List<Product> PRODUCTS = Arrays.asList(
			  new Product(1, "iPhone"),
		      new Product(2, "Apple Watch"),
		      new Product(3, "Apple iPad"),
		      new Product(4, "Apple iPod")
			);
	
	@GetMapping
	public List<Product> getAllProduct()
	{
		return PRODUCTS;
	}
	
	@PostMapping
	public String addProduct(@RequestBody Product product)
	{
		System.out.println("inside @PostMapping...");
		return "inside @PostMapping...";
	}
	
	@DeleteMapping("/{productId}")
	public String deleteProduct(@PathVariable Integer productId)
	{
		System.out.println("inside @DeleteMapping...");
		return "inside @DeleteMapping...";
	}
	
	@PutMapping
	public String updateProduct(@RequestBody Product product)
	{
		System.out.println("inside @PutMapping...");
		return "inside @PutMapping...";
	}
}
