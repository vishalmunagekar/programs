package com.app.SpringSecurity.Controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/v1/students")
public class StudentController {
	private static final List<Product> PRODUCTS = Arrays.asList(
			  new Product(1, "iPhone"),
		      new Product(2, "Apple Watch"),
		      new Product(3, "Apple iPad"),
		      new Product(4, "Apple iPod")
			);
	@GetMapping
	public List<Product> getAllProduct()
	{
		return PRODUCTS;
	}
	
	@GetMapping("/{productId}")
	public Product getProduct(@PathVariable Integer productId)
	{
		return PRODUCTS.stream()
				.filter((product) -> product.getProductId().equals(productId))
						.findFirst()
						.orElseThrow(() -> new IllegalStateException(
								"product " + productId + " does not exists"));
	}
}
