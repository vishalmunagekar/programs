package com.app.SpringSecurity.Pojo;

import java.util.Collection;
import java.util.Set;

import javax.persistence.*;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity(name = "User")
public class User implements UserDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer userId;
	private String username;
    private String password;
    
    @Column(length = 15)
	@Enumerated(EnumType.STRING)
    private UserRole role;
    
    @Transient
    private Set<? extends GrantedAuthority> grantedAuthorities;
    @Transient
    private boolean isAccountNonExpired;
    @Transient
    private boolean isAccountNonLocked;
    @Transient
    private boolean isCredentialsNonExpired;
    @Transient
    private boolean isEnabled;
    
    public User() {
    	System.out.println("User works...");
	}
    
	public User(String username, String password, UserRole role, Set<? extends GrantedAuthority> grantedAuthorities,
			boolean isAccountNonExpired, boolean isAccountNonLocked, boolean isCredentialsNonExpired,
			boolean isEnabled) {
		super();
		this.username = username;
		this.password = password;
		this.role = role;
		this.grantedAuthorities = grantedAuthorities;
		this.isAccountNonExpired = isAccountNonExpired;
		this.isAccountNonLocked = isAccountNonLocked;
		this.isCredentialsNonExpired = isCredentialsNonExpired;
		this.isEnabled = isEnabled;
	}

		@Override
	    public Collection<? extends GrantedAuthority> getAuthorities() {
	        return grantedAuthorities;
	    }

	    @Override
	    public String getPassword() {
	        return password;
	    }

	    @Override
	    public String getUsername() {
	        return username;
	    }

	    @Override
	    public boolean isAccountNonExpired() {
	        return isAccountNonExpired;
	    }

	    @Override
	    public boolean isAccountNonLocked() {
	        return isAccountNonLocked;
	    }

	    @Override
	    public boolean isCredentialsNonExpired() {
	        return isCredentialsNonExpired;
	    }

	    @Override
	    public boolean isEnabled() {
	        return isEnabled;
	    }

		public UserRole getRole() {
			return role;
		}

		public void setRole(UserRole role) {
			this.role = role;
		}

		public Set<? extends GrantedAuthority> getGrantedAuthorities() {
			return grantedAuthorities;
		}

		public void setGrantedAuthorities(Set<? extends GrantedAuthority> grantedAuthorities) {
			this.grantedAuthorities = grantedAuthorities;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public void setPassword(String password) {
			this.password = password;
		}

		public void setAccountNonExpired(boolean isAccountNonExpired) {
			this.isAccountNonExpired = isAccountNonExpired;
		}

		public void setAccountNonLocked(boolean isAccountNonLocked) {
			this.isAccountNonLocked = isAccountNonLocked;
		}

		public void setCredentialsNonExpired(boolean isCredentialsNonExpired) {
			this.isCredentialsNonExpired = isCredentialsNonExpired;
		}

		public void setEnabled(boolean isEnabled) {
			this.isEnabled = isEnabled;
		}

		public Integer getUserId() {
			return userId;
		}

		public void setUserId(Integer userId) {
			this.userId = userId;
		} 
	    

}
