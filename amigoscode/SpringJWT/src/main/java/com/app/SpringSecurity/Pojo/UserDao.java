package com.app.SpringSecurity.Pojo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface UserDao extends JpaRepository<User, Integer> {
	@Query(value = "SELECT u FROM User u WHERE u.username = ?1", nativeQuery=false)
	User AuthenticateUser(String username);
}
