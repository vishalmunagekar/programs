package com.app.SpringSecurity.Controller;

public class Product {
	private final Integer productId;
    private final String productName;

    public Product(Integer productId,
                   String productName) {
        this.productId = productId;
        this.productName = productName;
    }

    public Integer getProductId() {
        return productId;
    }

    public String getProductName() {
        return productName;
    }
}
