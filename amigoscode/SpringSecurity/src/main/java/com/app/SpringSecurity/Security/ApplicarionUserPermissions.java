package com.app.SpringSecurity.Security;

public enum ApplicarionUserPermissions {
	STUDENT_WRITE("student:write"),
	STUDENT_READ("student:read"),
	COURSE_READ("course:read"),
    COURSE_WRITE("course:write");
	
	private final String permission;
	
	private ApplicarionUserPermissions(String permission) {
		this.permission = permission;
	}

	public String getPermission() {
		return permission;
	}
}
