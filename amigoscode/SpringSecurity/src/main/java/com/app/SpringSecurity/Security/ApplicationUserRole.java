package com.app.SpringSecurity.Security;

import static com.app.SpringSecurity.Security.ApplicarionUserPermissions.*;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.authority.SimpleGrantedAuthority;

import com.google.common.collect.Sets;

public enum ApplicationUserRole {
	STUDENT(Sets.newHashSet()),
    ADMIN(Sets.newHashSet()),
    ADMINTRAINEE(Sets.newHashSet());
	
	private final Set<ApplicarionUserPermissions> permissions;
	
	private ApplicationUserRole(Set<ApplicarionUserPermissions> permissions) {
		this.permissions = permissions;
		
	}

	public Set<ApplicarionUserPermissions> getPermissions() {
		return permissions;
	}
	
	public Set<SimpleGrantedAuthority> getGrantedAuthorities(){
		Set<SimpleGrantedAuthority> permissions = this.getPermissions().stream()
				.map(permission -> new SimpleGrantedAuthority(permission.getPermission()))
				.collect(Collectors.toSet());
		 permissions.add(new SimpleGrantedAuthority("ROLE_" + this.name()));
		 System.out.println(permissions.toString());
		 System.out.println(this.name());
	     return permissions;
	}
}
