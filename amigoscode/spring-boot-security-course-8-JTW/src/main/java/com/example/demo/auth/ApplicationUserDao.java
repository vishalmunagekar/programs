package com.example.demo.auth;



import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface ApplicationUserDao extends  JpaRepository<User, Integer>{
	
	@Query("SELECT u FROM User u WHERE u.username = ?1")
	User selectApplicationUserByUsername(String username);

}
