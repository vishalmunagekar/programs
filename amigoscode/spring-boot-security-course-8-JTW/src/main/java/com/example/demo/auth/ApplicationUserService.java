package com.example.demo.auth;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import com.example.demo.security.ApplicationUserRole;


@Service
public class ApplicationUserService implements UserDetailsService {

	@Autowired
    private ApplicationUserDao applicationUserDao;
	
	@Autowired
	private PasswordEncoder passwordEncoder;


    @Override
    public UserDetails loadUserByUsername(String username) {
        User user =  applicationUserDao.selectApplicationUserByUsername(username);
        
        UserDetails dataBaseUser = new ApplicationUser(
                user.getUsername(),
                passwordEncoder.encode(user.getPassword()),
                ApplicationUserRole.valueOf(user.getRole().name()).getGrantedAuthorities(),
                true,
                true,
                true,
                true
        );
        return dataBaseUser;
    }
}
