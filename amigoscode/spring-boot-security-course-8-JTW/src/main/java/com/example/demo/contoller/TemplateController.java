package com.example.demo.contoller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.auth.ApplicationUserService;
import com.example.demo.jwt.AuthenticationResponse;
import com.example.demo.jwt.LoginRequest;
import com.example.demo.security.AuthService;

@RestController
@RequestMapping("/userlogin")
public class TemplateController {

	
	@Autowired
	private AuthService authService;
	
    @PostMapping("/login")
    public AuthenticationResponse getLogin(@RequestBody LoginRequest loginRequest) {
    	System.out.println(loginRequest);
        return authService.login(loginRequest);
    }

    @GetMapping("courses")
    public String getCourses() {
        return "courses";
    }
}
