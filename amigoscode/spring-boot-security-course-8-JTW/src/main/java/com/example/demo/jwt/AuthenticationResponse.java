package com.example.demo.jwt;


public class AuthenticationResponse {
	    private String authenticationToken;
	    private String expiresAt;
	    private String username;
	    
	    public AuthenticationResponse() {
			System.out.println("AuthenticationResponse() works..");
		}

		public AuthenticationResponse(String authenticationToken, String expiresAt,
				String username) {
			super();
			this.authenticationToken = authenticationToken;
			this.expiresAt = expiresAt;
			this.username = username;
		}

		public String getAuthenticationToken() {
			return authenticationToken;
		}

		public void setAuthenticationToken(String authenticationToken) {
			this.authenticationToken = authenticationToken;
		}

		public String getExpiresAt() {
			return expiresAt;
		}

		public void setExpiresAt(String expiresAt) {
			this.expiresAt = expiresAt;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}
}
