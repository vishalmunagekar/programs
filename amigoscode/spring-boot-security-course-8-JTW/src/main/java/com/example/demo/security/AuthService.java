package com.example.demo.security;

import java.time.LocalDate;
import java.util.Date;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import com.example.demo.jwt.AuthenticationResponse;
import com.example.demo.jwt.LoginRequest;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;

@Service
@Transactional
public class AuthService {

	@Resource(name="authenticationManager")
	private AuthenticationManager authenticationManager;
	
	public AuthService() {
		// TODO Auto-generated constructor stub
	}
	
	public AuthenticationResponse login(LoginRequest loginRequest)
    {
    	 //System.out.println(loginRequest);
         Authentication authenticate = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
    			 loginRequest.getUsername(),
    			 loginRequest.getPassword()
         ));
         
         String secretKey = "securesecuresecuresecuresecuresecuresecuresecuresecuresecuresecure";
         
         String token = Jwts.builder()
                 .setSubject(loginRequest.getUsername())
                 .claim("Authorization", authenticate.getAuthorities())
                 .setIssuedAt(new Date())
                 .setExpiration(java.sql.Date.valueOf(LocalDate.now().plusDays(2)))
                 .signWith(Keys.hmacShaKeyFor(secretKey.getBytes()))
                 .compact();
         
    	return new AuthenticationResponse(token, null, loginRequest.getUsername());
    }
	
}
